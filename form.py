from flask_wtf import FlaskForm
from wtforms import StringField, DateField, BooleanField, SelectField
from wtforms.validators import DataRequired, ValidationError
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
from flask import request
from google.cloud import datastore
from utils import *
datastore_client = datastore.Client()

def checkslashend(form, field):
    if field.data.count("/") != 1 or field.data[-1] != "/":
        raise ValidationError("Error with the name !")

def removeillegalCaractere(form, field):
    if "/" in field.data:
        raise ValidationError("You cant use the /")

def validate_name_exist(form, field):
    claims, error_message = auth(request.cookies.get("token"))
    if checkvalid(claims["user_id"], form.path.data + form.name.data):
        raise ValidationError("This name is already is the database")

def checkisvalid(form, field):
    claims, error_message = auth(request.cookies.get("token"))
    if not checkvalid(claims["user_id"], field.data):
        raise ValidationError("Path is wrong")


class AddDir(FlaskForm):
    path = StringField('path', validators=[checkisvalid])
    name = StringField('name', validators=[DataRequired(), validate_name_exist, checkslashend])


class AddFile(FlaskForm):
    path = StringField('path', validators=[checkisvalid])
    name = StringField('name', validators=[DataRequired(), removeillegalCaractere])
    file = FileField('file', validators=[FileRequired()])


class DelDir(FlaskForm):
    path = StringField('path', validators=[checkisvalid])
