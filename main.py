import os
from flask import Flask, render_template, redirect, url_for, request, send_file, abort
from utils import *
from form import *
import io
firebase_request_adapter = requests.Request()

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

@app.route('/path/<path:path>')
def root_with_path(path):
    return root_with_form(AddDir(), DelDir(), AddFile(), path)

@app.route('/path/')
def root_with_path_default():
    return root()

@app.route('/')
def root():
    return root_with_form(AddDir(), DelDir(), AddFile())

def root_with_form(form_add_dir, form_del_dir, form_add_file, prefix="/"):
    claims, error_message = auth(request.cookies.get("token"))
    list_b = None
    list_d = None
    user = None
    start_dir = None
    countsub = []
    if claims is not None:
        user = fetch_user(claims)
        if len(prefix) == 0 or prefix[0] != "/":
            prefix = "/"+prefix
        prefix = validprefix(user.key.name, prefix)
        list_b = list_blob(user.key.name, prefix, False)
        if len(list_b) == 0:
            create_empty_dir(user.key.name, "/")
            list_b = list_blob(user.key.name, prefix)
        countsub = groupbyandcount(list_b, "stockage/"+user.key.name+prefix)
        #subprocess
        list_b = filtersub_fct(list_b, prefix.count("/")+1)
        list_b = list(filter(lambda x: x.name != "stockage/"+user.key.name+prefix, list_b))
        list_d = list(filter(lambda x: x.name[-1] == "/", list_b))
        list_b = list(filter(lambda x: x.name[-1] != "/", list_b))

        start_dir = "stockage/"+user.key.name+prefix
        form_add_dir.path.data = prefix
        form_add_file.path.data = prefix
    return render_template(
        'index.html',
        user_data=claims, error_message=error_message, form_add_dir=form_add_dir, form_add_file=form_add_file,
        user=user, list_b=list_b, list_d=list_d, start_dir=start_dir, form_del_dir=form_del_dir, countsub=countsub)

@app.route("/adddir", methods=["POST"])
def adddir():
    form_add_dir = AddDir()

    claims, error_message = auth(request.cookies.get("token"))
    if claims is not None:
        if form_add_dir.name.data[-1] != "/":
            form_add_dir.name.data += "/"
        if form_add_dir.validate_on_submit():
            create_empty_dir(claims["user_id"], form_add_dir.path.data + form_add_dir.name.data)
            return redirect(url_for('root_with_path', path=validprefix(claims["user_id"], form_add_dir.path.data)))
        else:
            return root_with_form(form_add_dir, DelDir(), AddFile(), validprefix(claims["user_id"], form_add_dir.path.data))
    return redirect(url_for('root'))

@app.route('/addblobfile', methods=['POST'])
def addblobfile():
    form_add_file = AddFile()
    claims, error_message = auth(request.cookies.get("token"))
    if claims is not None:
        if form_add_file.validate_on_submit():
            f = form_add_file.file.data
            filename = secure_filename(form_add_file.name.data)
            blob = bucket.blob("stockage/" + claims["user_id"] + form_add_file.path.data + filename)
            blob.upload_from_string(f.read(), content_type='application/x-www-form-urlencoded;charset=UTF-8')
            return redirect(url_for('root_with_path',
                                    path=validprefix(claims["user_id"], form_add_file.path.data)))
        else:
            return root_with_form(AddDir(), DelDir(), form_add_file, validprefix(claims["user_id"], form_add_file.path.data))
    return redirect(url_for('root'))

@app.route("/delete", methods=["POST"])
def delete():
    form_del_dir = DelDir()

    claims, error_message = auth(request.cookies.get("token"))
    if claims is not None:
        if form_del_dir.validate_on_submit():
            deletedir(claims["user_id"], form_del_dir.path.data)
            return redirect(url_for('root_with_path', path=validprefix(claims["user_id"], form_del_dir.path.data[0:form_del_dir.path.data[:-1].rfind("/")]+"/")))
    return redirect(url_for('root'))

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html', title = '404'), 404

@app.route('/download/<path:path>')
def download(path):
    path = "/" + path
    claims, error_message = auth(request.cookies.get("token"))
    if claims is not None:
        l = list_blob(claims["user_id"], path)
        if len(l) == 0:
            abort(404)
        else:
            blob = l[0]
            return send_file(
                io.BytesIO(blob.download_as_bytes()),
                attachment_filename=blob.name[blob.name.rfind("/"):-1])
    abort(404)


@app.route('/allduplicatecurrent/')
def allduplicatecurrent_default():
    return allduplicatecurrent("")

@app.route('/allduplicatecurrent/<path:path>')
def allduplicatecurrent(path):
    path = "/" + path
    claims, error_message = auth(request.cookies.get("token"))
    if claims is not None:
        if len(path) == 0 or path[0] != "/":
            path = "/"+path
        prefix = validprefix(claims["user_id"], path)
        list_b = list_blob(claims["user_id"], prefix, True)
        list_b = findduplicate(list_b)
        start_dir = "stockage/"+claims["user_id"]+prefix
        user = fetch_user(claims)
        return render_template(
            'duplicatecurrent.html',
            user_data=claims, error_message=error_message, list_b=list_b, start_dir=start_dir, user=user)

    return redirect(url_for('root'))


@app.route('/allduplicate/')
def allduplicate():
    claims, error_message = auth(request.cookies.get("token"))
    if claims is not None:
        prefix = validprefix(claims["user_id"], "/")
        list_b = list_blob(claims["user_id"], prefix, False)
        list_b = findduplicate(list_b)
        user = fetch_user(claims)
        return render_template(
            'duplicateall.html',
            user_data=claims, error_message=error_message, list_b=list_b, user=user)
    return redirect(url_for('root'))


@app.route('/downloadpublic/<userid>/<sha>/<path:path>')
def downloadpublic(userid, sha, path):
    path = "/" + path
    l = list_blob(userid, path)
    if len(l) == 0:
        abort(404)
    else:
        blob = l[0]
        if blob.md5_hash == sha:
            return send_file(
                io.BytesIO(blob.download_as_bytes()),
                attachment_filename=blob.name[blob.name.rfind("/"):-1])
    abort(404)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
