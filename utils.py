from google.auth.transport import requests
import google.oauth2.id_token
from google.cloud import datastore, storage
from datetime import *
firebase_request_adapter = requests.Request()
datastore_client = datastore.Client()
storage_client = storage.Client()
bucket = storage_client.bucket("eu.artifacts.griffithtp1.appspot.com")

def auth(id_token):
    if id_token:
        try:
            return google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter), None
        except ValueError as exc:
            print(exc)
            return None, str(exc)
    return [None, None]

def fetch_user(user_claim):
    key = datastore_client.key('user', user_claim["user_id"])
    query = datastore_client.query(kind='user')
    query.add_filter("__key__", "=", key)
    users = list(query.fetch())
    if len(users) > 0:
        return users[0]
    entity = datastore.Entity(key=key)
    entity.update({
        "name": user_claim["name"]
    })
    datastore_client.put(entity)

    return entity


def filtersub_fct(l, count):
    return list(filter(lambda x: (x.name.count("/") == count or (x.name.count("/") == count + 1 and x.name[-1] == "/")), l))


def list_blob(userid, prefix, filtersub=True):
    count = prefix.count("/")+1
    l = list(storage_client.list_blobs(bucket.name, prefix="stockage/"+userid+prefix))
    if filtersub:
        l = filtersub_fct(l, count)
    return l


def create_empty_dir(userid, dir):
    blob = bucket.blob("stockage/"+userid+dir)
    blob.upload_from_string('', content_type='application/x-www-form-urlencoded;charset=UTF-8')

def checkvalid(userid, path):
    l = list_blob(userid, path)
    if len(l) == 0:
        return False
    return len(list(filter(lambda x: x.name == "stockage/"+userid+path, l))) != 0

def validprefix(userid, path):
    if len(path) == 0 or path[0] != "/" and path[-1] != "/":
        return "/"
    if checkvalid(userid, path):
        return path
    return "/"

def deletedir(userid, path):
    bucket.delete_blobs(list_blob(userid, path, False))

def groupbyandcount(data, prefix):
    tab = {}
    for d in data:
        name = d.name.replace(prefix, "")
        if "/" in name:
            name = name[0:name.find("/")+1]
            if name in tab:
                tab[name] += 1
            else:
                tab[name] = 0
    return tab

def findduplicate(list_b):
    tab = {}
    for d in list_b:
        if d.name[-1] != "/":
            code = d.md5_hash + str(d.size)
            if code in tab:
                tab[code].append(d)
            else:
                tab[code] = [d]
    return dict(filter(lambda elem: len(elem[1]) > 1, tab.items()))
