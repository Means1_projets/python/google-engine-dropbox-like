Flask==1.1.2
flask-wtf==0.14.3
google-cloud-datastore>=1.15.3,<2.0
google.cloud.storage
google-auth==1.28.0
requests==2.25.1