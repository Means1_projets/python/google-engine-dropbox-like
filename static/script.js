window.addEventListener('load', function () {
  document.getElementById('sign-out').onclick = function () {
    firebase.auth().signOut();
    setTimeout(function(){ location.reload(); }, 500);
  };

  // FirebaseUI config.
  var uiConfig = {
    signInSuccessUrl : "/",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ],
    // Terms of service url.
    tosUrl: 'localhost'
  };

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in, so display the "sign out" button and login info.
      document.getElementById('sign-out').hidden = false;
      document.getElementById('login-info').hidden = false;
      console.log(`Signed in as ${user.displayName} (${user.email})`);
      user.getIdToken().then(function (token) {

        document.cookie = "token="+token+ "; path=/"

      });
    } else {
      // User is signed out.
      // Initialize the FirebaseUI Widget using Firebase.
      var ui = new firebaseui.auth.AuthUI(firebase.auth());
      // Show the Firebase login button.
      ui.start('#firebaseui-auth-container', uiConfig);
      // Update the login state indicators.
      document.getElementById('sign-out').hidden = true;
      document.getElementById('login-info').hidden = true;
      // Clear the token cookie.
      document.cookie = "token=; path=/"
    }
  }, function (error) {
    console.log(error);
    alert('Unable to log in: ' + error)
  });
  form = document.getElementById('form-addblobfile')
  if (form != null){
    namefield = form.querySelector('#name')
    if (namefield != null){
      namefield.onchange = function(e) {
        name = namefield.value
        if (Array.from(document.getElementById('listelement').querySelectorAll('.element')).filter(t => t.text === name).length > 0){
          document.getElementById('hiddenwarning').hidden = false
          console.log("show")
        } else {
          console.log("hide")
          document.getElementById('hiddenwarning').hidden = true
        }
      };
    }

    filefield = form.querySelector('#file')
    if (filefield != null){
      filefield.onchange = function(e) {
        namefield.value = e.target.value.replace(/.*[\/\\]/, '')
        namefield.onchange(e)
      }
    }
  }

  var deleteModal = document.getElementById('deleteModal')
  if (deleteModal !== null)
    deleteModal.addEventListener('show.bs.modal', function (event) {
      // Button that triggered the modal
      var button = event.relatedTarget
      // Extract info from data-bs-* attributes
      var recipient = button.getAttribute('data-bs-path')
      // If necessary, you could initiate an AJAX request here
      // and then do the updating in a callback.
      //
      // Update the modal's content.
      var modalBodyInput = deleteModal.querySelector('.modal-body #path')
      console.log(recipient)
      modalBodyInput.value = recipient
    })

  var copylinkbutton = document.getElementsByClassName("copylinkbutton")
  if (copylinkbutton !== null)
    Array.from(copylinkbutton).forEach(function (param) {
      param.addEventListener("click", function (param) {
        text = param.target.parentElement.querySelector('.copylinkdata').value
        navigator.clipboard.writeText(window.document.documentURI+text.substring(1, text.length)).then(function() {
        }, function() {
          param.target.parentElement.querySelector('.copylinkdata').hidden = false
        });
      })
    })
});
